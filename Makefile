CXX=clang++
LINK=-lpthread -lrofl

CXXFLAGS+=-g -O2 -O3 -std=c++11


# File names
EXEC = benchmark
SOURCES = $(wildcard *.cc utils/*.cc)
OBJECTS = $(SOURCES:.cc=.o)
DEPS = $(SOURCES:.cc=.d)


# Main target
$(EXEC): $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(OBJECTS) -o $(EXEC) $(LINK)


%.o:%.cc
	$(CXX) $(CXXFLAGS) -c $(INCL) $< -o $@

clean:
	rm -f $(EXEC) $(OBJECTS) $(DEPS)

.PHONY: depend

depend: .depend
.depend: $(SOURCES)
	rm -f ./.depend
	$(CC) $(CFLAGS) -MM $^>>./.depend;

include .depend
