/***
Benchmark rule instalation.
***/

#include <string>
#include <vector>

#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/scoped_ptr.hpp>

#include "rule_generator.hh"
#include "timer.hh"
#include "log.h"
#include "controller.h"

#include <unistd.h>
#include "benchmark.h"
#include "rofl_coption_values.h"

#include "of_version.h"

#define COOLDOWN_TIME 20

using namespace std;

namespace benchmark {

static uint32_t CLEANING_DONE = 1212121211;
static uint32_t PREPARE_DONE = 1212121212;
static uint32_t BENCHMARK_DONE = 1313131313;

static dack::Logger LOG("benchmark");

void Benchmark::handle_ConnectionUp(SwitchConnection* sw_conn) {
    LOG(INFO) << "switch " << sw_conn->get_dpid() << " joined\n";
    if(sw_conn->get_dpid() == switch_dpid) {
        connection = sw_conn;
        sleep(1);
        prepareSwitch();
    }
}

void Benchmark::handle_error(openflow::cofmsg_error* msg) {
    printf("ERROR. Type: %d Code: %d\n", msg->get_err_type(), msg->get_err_code());
}

void Benchmark::handle_barrier_reply(openflow::cofmsg_barrier_reply* msg) {
    uint32_t xid = msg->get_xid();
    if(xid == CLEANING_DONE)
        return; // ignore, this barrier is only to instruct the switch not to reorder cleaning and installing new rules.
    if(xid >= PREPARE_DONE && xid < PREPARE_DONE+COOLDOWN_TIME) {
//        printf("waiting %d\n", xid);
        sleep(1);
        auto barrier = new openflow::cofmsg_barrier_request(
            ofproto::OFP_VERSION,
            xid+1
        );
        connection->send_message(barrier);     
    }
    else if(xid == PREPARE_DONE+COOLDOWN_TIME) {
        LOG(INFO) << "switch prepared\n";
        LOG(INFO) << "starting soon\n";
        sleep(1);
        startDataplane();
        sleep(2);
        startBenchmark();
    }
    else if(xid == BENCHMARK_DONE) {
        finishBenchmark();
	    sleep(1);
    	stopDataplane();
        FILE* f = fopen("/tmp/benchmark_done", "w");
        assert(f);
        fclose(f);
        exit(EXIT_SUCCESS);
    }
    else {
        logger->batchInstalled(xidGenerator->xidToID(xid), timer->getTime());
        sendNextBatch();
    }
}

inline void Benchmark::startDataplane() {
    if(!send_dp)
        return;

    int pid = fork();
    if (pid < 0) {
       LOG(WARN) << "Aborting. Could not fork! " << errno;
       exit(1);
    }
    if(pid == 0) {
       execl("/usr/sbin/tcpdump", "tcpdump", "-B", "200000", "-i", dst.c_str(), "-n", "-w", "/tmp/output.pcap", "-s", "0", (char *) 0);
       LOG(WARN) << "Error executing tcpdump " << errno;
       exit(1);
    }
    dump_pid = pid;
    printf("DUMP PID: %d\n", dump_pid);

    pid = fork();
    if (pid < 0) {
       LOG(WARN) << "Aborting. Could not fork! " << errno;
       exit(1);
    }
    if(pid == 0) {
        execl("/usr/bin/tcpreplay", "tcpreplay", "--pps=90000", "--pps-multi=300", "--loop=0", "--enable-file-cache", "--quiet", "--intf1", src.c_str(), dp_input.c_str(), (char *) 0);
        LOG(WARN) << "Error executing tcpreplay " << errno;
        exit(1);
    }

    replay_pid = pid;   
    printf("REPL PID: %d\n", replay_pid);  
}

inline void Benchmark::stopDataplane() {
    if(!send_dp)
        return;

    printf("KILL\n");
    kill(replay_pid, SIGINT);
    sleep(1);
    kill(dump_pid, SIGINT);
}

void Benchmark::prepareSwitch() {
    LOG(INFO) << "preparing a switch\n";

    //clean the flow table
    auto clear = cofflowmod(ofproto::OFP_VERSION);
    clear.set_command(OFPFC_DELETE);
    if(clear.get_version() == openflow10::OFP_VERSION)
        clear.set_out_port(openflow10::OFPP_NONE);
    else
        clear.set_table_id(0xff);
    connection->send_message(new cofmsg_flow_mod(ofproto::OFP_VERSION, 0, clear));
    auto barrier_clean = new openflow::cofmsg_barrier_request(
            ofproto::OFP_VERSION,
            CLEANING_DONE
        );
    connection->send_message(barrier_clean);
    sleep(2);

    auto fmod = cofflowmod(ofproto::OFP_VERSION);
    fmod.set_command(OFPFC_ADD);
    fmod.set_idle_timeout(OFP_FLOW_PERMANENT);
    fmod.set_hard_timeout(OFP_FLOW_PERMANENT);
    fmod.set_priority(1);

    cofmatch match(ofproto::OFP_VERSION);
    match.set_eth_type(0x800); // IP
    fmod.match = match;
    
    connection->send_message(new cofmsg_flow_mod(ofproto::OFP_VERSION, 0, fmod));

    LOG(INFO) << "drop rule sent\n";
    sleep(1);
    
    for(int i=0; i<initial_rules; i++) {
        connection->send_message(generator->getNextAdd());
    }

    LOG(INFO) << "initial rules installed\n";
    auto barrier = new openflow::cofmsg_barrier_request(
            ofproto::OFP_VERSION,
            PREPARE_DONE
        );

    connection->send_message(barrier);
}

void Benchmark::startBenchmark() {
    LOG(INFO) << "benchmark starting\n";
 
    timer->restartTimer();   
    for(int i=0; i<outstanding_batches; i++) {
        sendNextBatch();   
    }
}

void Benchmark::finishBenchmark() {
    double time = timer->getTime();
    cioloop::stop();
    logger->saveResults(output_file.c_str());
    LOG(INFO) << "Full benchmark time: " << time << " sec\n";
    LOG(ERROR) << "Rate: \t" << (float)benchmark_rules/time << "\t commands/sec\n";
    printf("%f\n", (float)benchmark_rules/time);
}

void Benchmark::sendNextBatch() {
    if(last_barrier_sent) {
        return;
    }

    int current_batch_id = xidGenerator->getNextId();

    //batches numbered from 1, so we take 1 to make sure the first sent batch is the first in the command line
    BatchDescription& currentBatch = batches[(current_batch_id - 1) % batches.size()];
   
    if(sent_rules + currentBatch.addCnt + currentBatch.delCnt + currentBatch.modCnt > benchmark_rules) {
        LOG(INFO) << "sending BENCHMARK_DONE barrier\n";
        auto barrier = new openflow::cofmsg_barrier_request(
              ofproto::OFP_VERSION,
              BENCHMARK_DONE
            );
        connection->send_message(barrier);
        last_barrier_sent = true;
        return;
    }

    for(int i=0; i<currentBatch.delCnt; i++) {
        connection->send_message(generator->getNextDel());
    }
    for(int i=0; i<currentBatch.modCnt; i++) {
        connection->send_message(generator->getNextMod());
    }
    for(int i=0; i<currentBatch.addCnt; i++) {
        connection->send_message(generator->getNextAdd());
    }

    if(currentBatch.delCnt + currentBatch.modCnt + currentBatch.addCnt == 0) {
        sent_rules += 1;    // allow for sending just barriers
    } else {
        sent_rules += (currentBatch.delCnt + currentBatch.modCnt + currentBatch.addCnt);
    }

    auto barrier = new openflow::cofmsg_barrier_request(
          ofproto::OFP_VERSION,
          xidGenerator->idToXid(current_batch_id)
        );
    logger->batchSent(current_batch_id, timer->getTime(), currentBatch);
    connection->send_message(barrier);
}

void Benchmark::handle_switch_to_ctrl(dpid_t dpid, cofmsg* msg) {
    switch(msg->get_type()) {
      case ofproto::OFPT_BARRIER_REPLY:
        return handle_barrier_reply(dynamic_cast<cofmsg_barrier_reply*>(msg));
      case ofproto::OFPT_ERROR:
        return handle_error(dynamic_cast<cofmsg_error*>(msg));
      default:
        LOG(WARN) << "unhandled message of type " << (int)msg->get_type() << "\n";
    }
}

void Benchmark::init_flags(cunixenv& flags) {
  flags.add_option(coption(ARG_REQUIRED, REQUIRE_VALUE, 'p', "dpid", "Dpid", "0"));
  flags.add_option(coption(ARG_OPTIONAL, REQUIRE_VALUE, 's', "batch_desc", "Batch desc", "1:1:0"));
  flags.add_option(coption(ARG_REQUIRED, REQUIRE_VALUE, 'r', "benchmark_rules", "Benchmark Rules", "0"));
  flags.add_option(coption(ARG_REQUIRED, REQUIRE_VALUE, 'o', "outstanding_batches", "Outstanding batches", "1"));
  flags.add_option(coption(ARG_REQUIRED, REQUIRE_VALUE, 'i', "initial_rules", "Initial rules", "0"));
  flags.add_option(coption(ARG_REQUIRED, REQUIRE_VALUE, 'x', "input", "Input file", ""));
  flags.add_option(coption(ARG_REQUIRED, REQUIRE_VALUE, 'y', "output", "Output file", ""));
  flags.add_option(coption(ARG_REQUIRED, REQUIRE_VALUE, 't', "output_ports", "Comma-separated list of output ports", ""));

  flags.add_option(coption(ARG_REQUIRED, REQUIRE_VALUE, 'd', "data_plane", "Send data plane traffic", "0"));
  flags.add_option(coption(ARG_OPTIONAL, REQUIRE_VALUE, 'f', "dp_input", "Data plane input file", ""));
  flags.add_option(coption(ARG_OPTIONAL, REQUIRE_VALUE, 'a', "src", "", ""));
  flags.add_option(coption(ARG_OPTIONAL, REQUIRE_VALUE, 'b', "dst", "", ""));
}

void Benchmark::configure(cunixenv& flags) {
    switch_dpid = boost::lexical_cast<long long int>(flags.get_arg("dpid"));
    {
      vector<string> batchDesc;
      string s = flags.get_arg("batch_desc");
      boost::split(batchDesc, s, boost::is_any_of("#"));
      for(string& batch : batchDesc) {
          int colon = batch.find(":");
          int colon2 = batch.find(":", colon + 1);
          BatchDescription newBatch;
          newBatch.addCnt = boost::lexical_cast<int>(batch.substr(0, colon));
          newBatch.delCnt = boost::lexical_cast<int>(batch.substr(colon + 1, colon2 - colon - 1));
          newBatch.modCnt = boost::lexical_cast<int>(batch.substr(colon2 + 1));
          batches.push_back(newBatch);
      }
    }

    benchmark_rules = boost::lexical_cast<int>(flags.get_arg("benchmark_rules"));
    outstanding_batches = boost::lexical_cast<int>(flags.get_arg("outstanding_batches"));
    initial_rules = boost::lexical_cast<int>(flags.get_arg("initial_rules"));
    output_file = flags.get_arg("output");
    input_file = flags.get_arg("input"); 

    send_dp = boost::lexical_cast<bool>(flags.get_arg("data_plane"));
    src = flags.get_arg("src");
    dst = flags.get_arg("dst");
    dp_input = flags.get_arg("dp_input");
    
    sent_rules = 0;
    last_barrier_sent = false;

    vector<int> output_ports;
    {
        string s = flags.get_arg("output_ports");
        vector<string> ports_str;
        boost::split(ports_str, s, boost::is_any_of(","));
        for(string& port : ports_str) {
            output_ports.push_back(boost::lexical_cast<int>(port));
        }
    }

    generator->readData(input_file.c_str(), output_ports, initial_rules);
}

}
