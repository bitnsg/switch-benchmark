#ifndef H_BENCHMARK
#define H_BENCHMARK

#include <rofl/common/openflow/cofhelloelemversionbitmap.h>
#include <rofl/platform/unix/cunixenv.h>

#include <rofl/common/openflow/messages/cofmsg_error.h>
#include <rofl/common/openflow/messages/cofmsg_barrier.h>

#include "logger.hh"
#include "timer.hh"

#include "log.h"
#include "controller.h"
#include <rofl/platform/unix/cunixenv.h>
#include "rule_generator.hh"
#include <boost/scoped_ptr.hpp>

namespace benchmark
{
using namespace rofl;



class XIDGenerator {
public:
    XIDGenerator() : currentBatchId(1) {}

    int xidToID(uint32_t xid) {
        return (xid - 47) / 100;
    }
    
    uint32_t idToXid(int batchId) {
        return 100 * batchId + 47;
    }

    uint32_t getNextId() {
        return currentBatchId++;
    }

private:
    int currentBatchId;
};


class Benchmark: public Controller {
public:
    Benchmark() : xidGenerator(new XIDGenerator()),
                  generator(new RuleGenerator()),
                  timer(new Timer()),
                  logger(new Logger()){}

    void init_flags(cunixenv& flags);
    void configure(cunixenv& flags);

    void install() {}

private:
    long long int switch_dpid;
    int benchmark_rules;
    int outstanding_batches;
    int initial_rules;
    string output_file;
    string input_file;

    int sent_rules;

    vector<BatchDescription> batches;

    boost::scoped_ptr<XIDGenerator> xidGenerator;
    boost::scoped_ptr<RuleGenerator> generator;
    boost::scoped_ptr<Timer> timer;
    boost::scoped_ptr<benchmark::Logger> logger;
    
    SwitchConnection* connection;
    bool last_barrier_sent;

    void prepareSwitch();
    void startBenchmark();
    void finishBenchmark();
    void sendNextBatch();
    void startDataplane();
    void stopDataplane();

    virtual void handle_ConnectionUp(SwitchConnection* sw_conn);
    virtual void handle_switch_to_ctrl(dpid_t dpid, cofmsg* msg);
    
    void handle_error(openflow::cofmsg_error*);
    void handle_barrier_reply(openflow::cofmsg_barrier_reply* msg);

    int dump_pid, replay_pid;
    string src, dst;
    string dp_input;
    bool send_dp;
};

}
#endif
