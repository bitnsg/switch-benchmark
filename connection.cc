/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "connection.h"
#include "log.h"
#include "of_version.h"

using namespace rofl;

namespace dack {
static Logger LOG("Connection");


Connection::Connection() :
  rofsock(this) {
}

Connection::~Connection()
{
}

void Connection::handle_timeout(
    int opaque, void *data)
{

}

void Connection::handle_connect_refused(crofsock* endpnt) {
  LOG(ERROR) << "Connection refused" << "\n";
}

void Connection::handle_connect_failed(crofsock* endpnt) {
  LOG(ERROR) << "Connection failed" << "\n";
}

void Connection::handle_closed(crofsock* endpnt) {
  LOG(ERROR) << "Connection closed" << "\n";
}

void Connection::handle_connected(crofsock* endpnt) {
  LOG(DEBUG) << "Connected" << "\n";
  send_hello_message();

}


void Connection::handle_echo_request(openflow::cofmsg_echo_request const& msg) {
  cmemory body(0);
  openflow::cofmsg_echo_reply *reply =
    new rofl::openflow::cofmsg_echo_reply(
        msg.get_version(),
        msg.get_xid(), //env->get_async_xid(this),
        body.somem(), body.memlen());

  LOG(DEBUG) << "Replying with echo response\n";
  rofsock.send_message(reply);
}


void Connection::send_hello_message()
{
  cmemory body(0);
  openflow::cofmsg_hello *hello =
    new rofl::openflow::cofmsg_hello(
        ofproto::OFP_VERSION,
        0,
        body.somem(), body.memlen());
  LOG(DEBUG) << "Sending hello message" << "\n";
  rofsock.send_message(hello);
}

}
