/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef H_CONNECTION
#define H_CONNECTION

#include <map>
#include <string>

#include "rofl/common/crofchan.h"

#include "rofl/common/ciosrv.h"
#include "rofl/common/logging.h"

#include "log.h"

namespace dack {

class Connection :
  public rofl::ciosrv,
  public rofl::crofsock_env
{

  protected: // data structures

    rofl::crofsock rofsock;

  public: // methods
    Connection();

    void accept(
        rofl::csocket::socket_type_t socket_type,
        rofl::cparams const& socket_params,
        int newsd) {
      rofsock.accept(socket_type, socket_params, newsd);
    }

    void connect(
        rofl::csocket::socket_type_t socket_type,
        rofl::cparams const& socket_params) {
      rofsock.connect(socket_type, socket_params);
    }

    virtual void send_message(rofl::openflow::cofmsg* msg) {
      rofsock.send_message(msg);
    }

    virtual ~Connection();
    
  protected:
    virtual void handle_connect_failed(rofl::crofsock* endpnt);
    virtual void handle_connect_refused(rofl::crofsock* endpnt);
    virtual void handle_connected(rofl::crofsock* endpnt);
    virtual void handle_closed(rofl::crofsock* endpnt);

  protected:
    virtual void handle_echo_request(rofl::openflow::cofmsg_echo_request const& msg);
    virtual void send_hello_message();

    virtual void handle_timeout(int opaque, void *data = NULL);


  public:

    friend std::ostream&
      operator<< (std::ostream& os, Connection const& ctl) {
        os << "Connection()";
        return os;
      };

};

}
#endif
