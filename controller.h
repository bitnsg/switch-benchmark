#ifndef H_CONTROLLER
#define H_CONTROLLER

#include "switch_connection.h"
#include "rofl/common/openflow/cofhelloelemversionbitmap.h"
#include "log.h"
#include <map>

using namespace rofl;
using namespace std;
using namespace dack;

static dack::Logger _LOG("controller");

class Controller:
    public csocket_owner,
    public SwitchProxyCallback
{
  public:
    cparams sock_params;

    virtual void handle_accepted(csocket& socket) {
      _LOG(INFO) << "accepted socket" << "\n";
    };
    virtual void handle_accept_refused(csocket& socket) {
      _LOG(INFO) << "accept refused" << "\n";
    };
    virtual void handle_connected(csocket& socket) {
      _LOG(INFO) << "connected socket" << "\n";
    };
    virtual void handle_connect_refused(csocket& socket) {
      _LOG(INFO) << "connect refused" << "\n";
    };
    virtual void handle_connect_failed(csocket& socket) {
      _LOG(INFO) << "connect failed" << "\n";
    };
    virtual void handle_read(csocket& socket) {
      _LOG(DEBUG) << "handle read" << "\n";
    };
    virtual void handle_write(csocket& socket) {
      _LOG(DEBUG) << "handle write" << "\n";
    };
    virtual void handle_closed(csocket& socket) {
      _LOG(INFO) << "socket closed" << "\n";
    };
    
    virtual void listen() {
      csocket* socket=csocket::csocket_factory(csocket::SOCKET_TYPE_PLAIN, this);
      _LOG(INFO)
        << "Listening to "
        << sock_params.get_param(csocket::PARAM_KEY_LOCAL_HOSTNAME).get_string()
        << ":" 
        << sock_params.get_param(csocket::PARAM_KEY_LOCAL_PORT).get_string()
        << "\n";
      socket->listen(sock_params);
    }
    
    virtual void handle_listen(csocket& socket, int newsd) {
      _LOG(DEBUG) << "Have socket in listen state, accepting " << newsd << "\n";
      SwitchConnection* conn = new SwitchConnection(this);
      conn->accept(socket.get_socket_type(),socket.get_socket_params(), newsd); 
    };

    virtual void handle_ConnectionUp(SwitchConnection* sw_conn) {
      uint64_t dpid = sw_conn->get_dpid();
      _LOG(INFO) << "ConnectionUp from " << dpid << "\n";
    }

    virtual void run() {
      listen();
      cioloop::run();
    }
};


#endif
