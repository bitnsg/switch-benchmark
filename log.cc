#include "log.h"
#include <iomanip>

#include <ostream>

namespace dack {
struct NullStream : std::ostream {
      NullStream() : std::ios(0), std::ostream(0) {}
};

template<typename T>
NullStream& operator<<(NullStream& s, const T x) {return s;}
static NullStream nullstream;

std::string TextLevels[] = {
  "INSANE",
  "DEBUG",
  "INFO",
  "WARNING",
  "ERROR",
};

LogLevel Logger::level;

void Logger::set_level(const LogLevel level) {
  Logger::level = level;
}

std::ostream& VLOG(const LogLevel level) {
  if (Logger::level > level) return nullstream;
  return (std::cout << TextLevels[level] << " ");
}

std::ostream& Logger::operator()(const LogLevel level) {
  if (Logger::level > level) return nullstream;
  return (std::cerr
          << "[" << std::setw(15) << _name << std::setw(0) << "]"
          << " " << std::setw(5) << TextLevels[level] << std::setw(0) << ": ").flush();
}

}
