#include<iostream>
#include<map>

#ifndef _H_LOG
#define _H_LOG
namespace dack {

enum LogLevel {
  INSANE,
  DEBUG,
  INFO,
  WARN,
  ERROR,
};

std::ostream& VLOG(const LogLevel level);

class Logger
{
  private:
    std::string _name;
  public:
    static LogLevel level;
    Logger(const std::string& name): _name(name){};
    std::ostream& operator ()(const LogLevel level);
    static void set_level(const LogLevel level);
};

}
#endif
