#include "logger.hh"
#include <stdio.h>
#include <assert.h> 

namespace benchmark {

void Logger::batchSent(int batchID, double time, BatchDescription batch) {
    assert(log.find(batchID) == log.end());
    BatchRecord& record = log[batchID];
    record.startTime = time;
    record.endTime = -1;
    record.batchDescription = batch;
}

void Logger::batchInstalled(int batchID, double time) {
    assert(log.find(batchID) != log.end());   
    log[batchID].endTime = time;
}

double Logger::getBatchTime(int batchID) {
    assert(log.find(batchID) != log.end());
    return log[batchID].endTime - log[batchID].startTime;
}

void Logger::saveResults(const char* filename) {
    FILE *pFile = fopen(filename, "w");
    if (!pFile) throw std::runtime_error("could not open output file");
    for(auto& record : log) {
        fprintf(pFile, "%d\t%lf\t%lf\t%d:%d:%d\n", record.first, record.second.startTime, record.second.endTime,
                                            record.second.batchDescription.addCnt,
                                            record.second.batchDescription.delCnt,
                                            record.second.batchDescription.modCnt);

    }
}

}
