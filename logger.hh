#ifndef LOGGER_H
#define LOGGER_H

#include <map>

using namespace std;
namespace benchmark {

struct BatchDescription {
    int addCnt;
    int delCnt;
    int modCnt;
};

struct BatchRecord {
    double startTime;
    double endTime;
    BatchDescription batchDescription;
};

class Logger {
public:
    void batchSent(int batchID, double time, BatchDescription batch);
    void batchInstalled(int batchID, double time);
    
    double getBatchTime(int batchID);
    void saveResults(const char* filename);
    
private:
    map<int, BatchRecord> log;
};

}
#endif
