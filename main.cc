#include "benchmark.h"
#include "log.h"
#include <rofl/platform/unix/cunixenv.h>

using namespace benchmark;
static dack::Logger LOG("main");

int main(int argc, char** argv) {
    rofl::cunixenv FLAGS(argc, argv);

    Benchmark benchmark; //(bitmap);
    benchmark.sock_params = csocket::get_default_params(csocket::SOCKET_TYPE_PLAIN);
    benchmark.sock_params.set_param(csocket::PARAM_KEY_LOCAL_PORT).set_string() = "6633";
    
    dack::Logger::set_level(DEBUG);
//    dack::Logger::set_level(ERROR);
    logging::set_debug_level("warn");
    LOG(INFO) << "Starting benchmark\n";
    
    benchmark.init_flags(FLAGS);
    FLAGS.parse_args();
    benchmark.configure(FLAGS);

    benchmark.run();
}
