#!/usr/bin/python3
import csv
import argparse
from collections import namedtuple
from pypacker import ppcap
from pypacker.layer12 import ethernet
from pypacker.layer3 import ip

def ParseCommandLine():
    parser = argparse.ArgumentParser()
    parser.add_argument('--OUTPUT_FILE', default="/tmp/results")
    parser.add_argument('--DATA_PCAP', default="/tmp/output.pcap")
    parser.add_argument('--CTRL_PLANE_LOG', default="/tmp/output")
    parser.add_argument('--INITIAL_RULES', default=300)
    parser.add_argument('--TIMER_RESTART')
    return parser.parse_args()

CONFIG = ParseCommandLine()

CtrlPlaneEntry = namedtuple("CtrlPlaneEntry", ("flow_id", "start_time", "end_time"))
BatchEntry = namedtuple("BatchEntry", ("batch_id", "src_ip", "dst_ip"))
FlowEntry = namedtuple("FlowEntry", ("flow_id", "start_time", "data_time", "ctrl_time", "packet"))

INITIAL_FLOW_ID = 1
BATCH_SIZE = 1

def IntToIp(ip):
    return "%d.%d.%d.%d" % ((ip >> 24) & 0xff, (ip >> 16) & 0xff, (ip >> 8) & 0xff, ip & 0xff)

class Results:

    @staticmethod
    def ReadModInput(input_file):
        batches = []
        for src in range(100 + int(CONFIG.INITIAL_RULES), 10000):
            srcIp = IntToIp(src)
            dstIp = IntToIp(src + 100000)
            batches.append(BatchEntry(src, srcIp, dstIp))
        return batches        

    @staticmethod
    def ReadDataPcap(input_file):
        data_plane_start = {}
        data_plane_end = {}
        for tstamp, buf in ppcap.Reader(filename=input_file):
            tstamp = tstamp / 1e9 # convert to seconds
            packet = ethernet.Ethernet(buf)
            src = packet[ip.IP].src_s
            dst = packet[ip.IP].dst_s
            p = (src, dst)
            if p not in data_plane_start:
                data_plane_start[p] = tstamp
            data_plane_end[p] = tstamp
        return (data_plane_start, data_plane_end)

    @staticmethod                      
    def ReadCtrlPlane(ctrl_plane_log):
        control_plane = []
        with open(ctrl_plane_log, 'r') as f:
            csvreader = csv.reader(f, delimiter='\t')
            for row in csvreader:
                for x in range(1, BATCH_SIZE+1):
                    control_plane.append(CtrlPlaneEntry(x + BATCH_SIZE * (int(row[0]) - INITIAL_FLOW_ID), float(row[1]), float(row[2])))
        return control_plane

    @staticmethod
    def CombineResults(time_offset, batches, control_plane, data_plane, output_file):
        results = []
        experiment_start_time = time_offset + control_plane[0].start_time
        for ctrl in control_plane:
            add = batches[ctrl.flow_id - INITIAL_FLOW_ID]
            p = (add.src_ip, add.dst_ip)
            if p not in data_plane:
                continue

            start_time = time_offset + ctrl.start_time
            ctrl_time = time_offset + ctrl.end_time
            data_time = data_plane[p]
            results.append(FlowEntry(ctrl.flow_id, start_time, data_time, ctrl_time, p))

        with open(output_file, 'w') as f:
#            experiment_start_time = results[0].start_time
            f.write("\t".join([
                "flowID", "packet", "dataplane", "ctrlplane", 
                "DP delay", "Barrier interarrival", "sent time", 
                "sent time", "dataplane", "ctrlplane\n"]))

            for i, result in enumerate(results):
                dp_delay = result.ctrl_time - result.data_time
                if i > 0:
                    barrier_interarrival= result.ctrl_time - results[i-1].ctrl_time
                else:
                    barrier_interarrival = 0

                columns = [
                    result.flow_id, result.packet, result.data_time, result.ctrl_time,
                    dp_delay, barrier_interarrival, result.start_time,
                    result.start_time - experiment_start_time, result.data_time - experiment_start_time, result.ctrl_time - experiment_start_time
                    ]
                f.write("\t".join(str(x) for x in columns[:2]))
                f.write("\t")
                f.write("\t".join("{0:.5f}".format(x) for x in columns[2:]))
                f.write("\n")


if __name__ == "__main__":
    time_offset = float(CONFIG.TIMER_RESTART)
    control_plane = Results.ReadCtrlPlane(CONFIG.CTRL_PLANE_LOG)
    batches = Results.ReadModInput(None)
    data_plane_start, data_plane_end = Results.ReadDataPcap(CONFIG.DATA_PCAP)
    Results.CombineResults(time_offset, batches, control_plane, data_plane_start, CONFIG.OUTPUT_FILE+"_start")
    Results.CombineResults(time_offset, batches, control_plane, data_plane_end, CONFIG.OUTPUT_FILE+"_end")
