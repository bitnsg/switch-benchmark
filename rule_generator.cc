#include "rule_generator.hh"

#include "of_version.h"
#include <stdio.h>
#include <iostream>
namespace benchmark {

cofmsg_flow_mod* RuleGenerator::getNextAdd() {
    return adds.at(nextAdd++);
}

cofmsg_flow_mod* RuleGenerator::getNextDel() {
    return dels.at(nextDel++);
}

cofmsg_flow_mod* RuleGenerator::getNextMod() {
    return mods.at(nextMod++);
}

cofmsg_flow_mod* RuleGenerator::createFlowMod(uint16_t command, uint32_t nw_src, uint32_t nw_dst, int outport) {
    cofinstructions inst(ofproto::OFP_VERSION);
    cofactions actions(ofproto::OFP_VERSION);
    if(command != ofproto::OFPFC_DELETE_STRICT && outport != -1) {
        if(inst.ofp_version == rofl::openflow10::OFP_VERSION)
            actions.append_action_output(outport);
        else
            inst.set_inst_apply_actions().get_actions().append_action_output(outport);
    }

    cofmatch match(ofproto::OFP_VERSION);
    match.set_eth_type(0x800); // IP
    if(match.get_version() == rofl::openflow10::OFP_VERSION) {
        match.set_matches().add_match(coxmatch_ofx_nw_src(nw_src));
        match.set_matches().add_match(coxmatch_ofx_nw_dst(nw_dst));
    }
    else {
        match.set_matches().add_match(coxmatch_ofb_ipv4_src(nw_src));
        match.set_matches().add_match(coxmatch_ofb_ipv4_dst(nw_dst));
    }
    cofflowmod fmod = cofflowmod(ofproto::OFP_VERSION);
    fmod.set_command(command);
    fmod.set_idle_timeout(OFP_FLOW_PERMANENT);
    fmod.set_hard_timeout(OFP_FLOW_PERMANENT);
    fmod.set_priority(OFP_DEFAULT_PRIORITY); //+OFP_DEFAULT_PRIORITY-nw_src);
    fmod.set_buffer_id(-1);
    if(fmod.get_version() == rofl::openflow10::OFP_VERSION) {
        fmod.actions = actions;
        fmod.set_out_port(rofl::openflow10::OFPP_NONE);
    }
    else {
        fmod.instructions = inst;
    }
    fmod.match = match;

    return new cofmsg_flow_mod(ofproto::OFP_VERSION, 0, fmod);
}

void RuleGenerator::readData(const char* filename, std::vector<int> outports, int initial_cnt) {
    nextAdd = 0;
    nextDel = 0;
    nextMod = 0;

    int dl_type;
    uint32_t nw_src;
    uint32_t nw_dst;
    int outport;

    addCnt = 15000;
    delCnt = 15000;
    modCnt = 10000;

    dl_type = 0x800;

    int x = 100;
    int y = 100;
    int z = 1;

    for(int i=0; i<addCnt; i++) {
        assert(dl_type == 0x800);
        adds.push_back(createFlowMod(ofproto::OFPFC_ADD, x, x+100000, outports[0]));
        x++;
   }

    for(int i=0; i<delCnt; i++) {
        assert(dl_type == 0x800);
        dels.push_back(createFlowMod(ofproto::OFPFC_DELETE_STRICT, y, y+100000, -1));
        y++;
   }

    if(outports.size() == 1)
        return;

    y = 100;
    for(int i=0; i<modCnt; i++) {
        assert(dl_type == 0x800);
        mods.push_back(createFlowMod(ofproto::OFPFC_MODIFY_STRICT, y, y+100000, outports[z]));
        y++;
        if(y==100 + initial_cnt) {
            y=100;
	        z=(z+1)%outports.size();
        }
   }
}

}
