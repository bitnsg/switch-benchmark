#ifndef RULE_GENERATOR_H
#define RULE_GENERATOR_H

#include <rofl/common/openflow/messages/cofmsg_flow_mod.h>
#include <vector>

namespace benchmark {
using namespace rofl::openflow;

class RuleGenerator {


public:
    void readData(const char* filename, std::vector<int> outports, int initial_cnt);
    cofmsg_flow_mod* getNextAdd();
    cofmsg_flow_mod* getNextDel();
    cofmsg_flow_mod* getNextMod();

private:
    cofmsg_flow_mod* createFlowMod(uint16_t command, uint32_t nw_src, uint32_t nw_dst, int outport);

    int addCnt, delCnt, modCnt;
    int nextAdd, nextDel, nextMod;

    std::vector<cofmsg_flow_mod*> adds, dels, mods;
};

}
#endif
