/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "switch_connection.h"
#include "log.h"
#include "of_version.h"

using namespace rofl;

namespace dack {

static Logger LOG("SwitchConnection");


SwitchConnection::SwitchConnection(
    SwitchProxyCallback* callback):
  Connection(), callback(callback) {
}

SwitchConnection::~SwitchConnection()
{
}



void SwitchConnection::handle_connected(crofsock* endpnt) {
  LOG(INFO) << "Connected" << "\n";
  state = HELLO_SENT;
  send_hello_message();
  send_features_request();
}


void
SwitchConnection::recv_message(
    crofsock* endpoint, rofl::openflow::cofmsg *msg)
{
    LOG(INSANE) << "Received message of type " << int(msg->get_type()) << "\n";
    assert(msg->get_type() == ofproto::OFPT_HELLO || msg->get_version() == ofproto::OFP_VERSION);
    switch (msg->get_type()) {
      case ofproto::OFPT_HELLO:
          LOG(DEBUG) << "received hello\n";
          break;
      case ofproto::OFPT_FEATURES_REPLY:
            if (state == HELLO_SENT) {
              handle_features_reply(dynamic_cast<rofl::openflow::cofmsg_features_reply
                    const&>(*msg));
            } else {
              return callback->handle_switch_to_ctrl(dpid, msg);
            }
          break;
      case ofproto::OFPT_ECHO_REQUEST:
          handle_echo_request(dynamic_cast<rofl::openflow::cofmsg_echo_request const&>(*msg));
          break;
      default:
          return callback->handle_switch_to_ctrl(dpid, msg);
    }
    delete msg;
}

void SwitchConnection::handle_features_reply(
    const rofl::openflow::cofmsg_features_reply& reply) {
  dpid = reply.get_dpid();
  state = ESTABLISHED;
  callback->handle_ConnectionUp(this);
}


void SwitchConnection::send_features_request()
{
  rofl::openflow::cofmsg_features_request* request =
    new rofl::openflow::cofmsg_features_request(
        ofproto::OFP_VERSION,
        0);
  LOG(INFO) << "Sending reatures request" << "\n";
  rofsock.send_message(request);
}

}
