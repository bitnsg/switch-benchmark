/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef H_SWITCH_CONNECTION
#define H_SWITCH_CONNECTION

#include <map>
#include <string>

#include "log.h"
#include "connection.h"
#include "of_types.h"

namespace dack {

enum ConnectionState {
  UNKNOWN = 0,
  HELLO_SENT = 1,
  ESTABLISHED = 2,
};

class SwitchConnection;

class SwitchProxyCallback {
  public:
  virtual void handle_switch_to_ctrl(dpid_t dpid, rofl::openflow::cofmsg* msg) = 0;
  virtual void handle_ConnectionUp(SwitchConnection* connection) = 0;
};

class SwitchConnection :
  public Connection
{

  private: // data structures

    ConnectionState state;
    dpid_t dpid;
    SwitchProxyCallback* callback;

  public: // methods
    SwitchConnection(SwitchProxyCallback* callback);

    void accept(
        rofl::csocket::socket_type_t socket_type,
        rofl::cparams const& socket_params,
        int newsd) {
      state = UNKNOWN;
      rofsock.accept(socket_type, socket_params, newsd);
    }

    virtual ~SwitchConnection();
    virtual dpid_t get_dpid() {return dpid;};
  protected:
    virtual void handle_connected(rofl::crofsock* endpnt);

    virtual void recv_message(
          rofl::crofsock *endpoint,
          rofl::openflow::cofmsg *msg
    );

    virtual void send_features_request();
    virtual void handle_features_reply(const rofl::openflow::cofmsg_features_reply& reply);
  public:

    friend std::ostream&
      operator<< (std::ostream& os, SwitchConnection const& ctl) {
        os << "SwitchConnection(" << ctl.dpid << ")";
        return os;
      };



};


}
#endif
