#ifndef TIMER_H
#define TIMER_H

#include <sys/time.h>

class Timer {
public:
    inline void restartTimer() {
        gettimeofday(&startTime, NULL);
        fprintf(stderr, "timer restart: %f\n", startTime.tv_sec + startTime.tv_usec/1000000.0);
    }

    inline double getTime() {
        timeval endTime;
        gettimeofday(&endTime, NULL);
        return (endTime.tv_sec - startTime.tv_sec) + (endTime.tv_usec - startTime.tv_usec) / 1000000.0;
    }

private:
    timeval startTime;
};

#endif
