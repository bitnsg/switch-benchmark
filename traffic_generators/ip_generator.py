#!/usr/bin/python

from scapy.all import *
import argparse
import csv

def ParseCommandLine():
    parser = argparse.ArgumentParser()
    parser.add_argument('--TO_FILE', default=1, type=int,
                        help='Write packets to a pcap file or send directly to a network interface.')
    parser.add_argument('--OUTPUT_FILE', 
                        help='Pcap file where packets should be stored.')
    parser.add_argument('--INTERFACE', default="eth4", 
                        help='A network interface used for sending packets.')
    parser.add_argument('--MULTIPLICATION_LEVEL', default=1, type=int, 
                        help='How many times should the trace be repeated. Series ids are stored in each packet.')
    parser.add_argument('--LOOP', default=0, type=int, 
                        help='Should packets be sent in a loop until canceled.')
    parser.add_argument('--INITIAL_SRC_IP', default=400, type=int,
                        help='Integer representation of the source ip of the first generated packet.')
    parser.add_argument('--PACKET_COUNT', default=300, type=int, 
                        help='Number of packets to generate.')
    return parser.parse_args()

CONFIG = ParseCommandLine()

def IntToIp(ip):
    return "%d.%d.%d.%d" % ((ip >> 24) & 0xff, (ip >> 16) & 0xff, (ip >> 8) & 0xff, ip & 0xff)

class Sender:

    @staticmethod
    def GeneratePackets():
        traffic = []
        packets = []
        adds = -1
        row_no = 0

        for packet_no in range(CONFIG.PACKET_COUNT):
            src = packet_no + CONFIG.INITIAL_SRC_IP
            src_ip = IntToIp(src)
            dst_ip = IntToIp(src + 100000)
            packet = Ether()/IP(src = src_ip, dst = dst_ip)/TCP()/("%d" % packet_no)
            packets.append(packet)

        return packets


    @staticmethod
    def MultiplyPackets(packets, multiplication):
        new_packets = []
        for x in range(multiplication):
            for p in packets:
                new_packets.append(p/("%6d" % x))
        return new_packets
            

    @staticmethod
    def StartSending(packets, interface, loop, timestep):
        print "Start sending"
        sendp(packets, iface=interface, inter=timestep, loop=loop)


if __name__ == '__main__':
    packets = Sender.GeneratePackets()
    new_packets = Sender.MultiplyPackets(packets, CONFIG.MULTIPLICATION_LEVEL)
    if CONFIG.TO_FILE:
        if not CONFIG.OUTPUT_FILE:
            print "Specify an output file or send directly to a network interface."
            exit(1)
        wrpcap(CONFIG.OUTPUT_FILE, new_packets)
    else:
        Sender.StartSending(new_packets, CONFIG.INTERFACE, CONFIG.LOOP, 0.0001)

